class Temperature
  # TODO: your code goes here!
  # f = c * 9/5 + 32
  #
  def initialize(h)
    @temp = h
  end

  def in_fahrenheit
    return @temp[:f] if @temp.keys[0] == :f
    Temperature.ctof(@temp[:c])
  end

  def in_celsius
    return @temp[:c] if @temp.keys[0] == :c
    Temperature.ftoc(@temp[:f])
  end

  def self.ctof(temp)
    temp * 9 / 5.0 + 32
  end

  def self.ftoc(temp)
    (temp - 32) * 5 / 9.0
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(f: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    super(c: temp)
  end
end
