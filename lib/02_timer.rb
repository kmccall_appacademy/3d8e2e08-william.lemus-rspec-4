class Timer
  attr_accessor :seconds
  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    minutes_text = display_unit((seconds / 60) % 60)
    hours_text = display_unit(seconds / 3600)
    seconds_text = display_unit(seconds % 60)
    "#{hours_text}:#{minutes_text}:#{seconds_text}"
  end

  def display_unit(num)
    return "0#{num}" if num < 10
    num
  end
end
