class Dictionary
  # TODO: your code goes here!
  def initialize(h = {})
    @definitions = h
  end

  def entries
    @definitions
  end

  def keywords
    @definitions.keys.sort
  end

  def add(entry)
    hash = hashify(entry)
    hash_key = hash.keys[0]
    @definitions[hash_key] = hash[hash_key]
  end

  def hashify(entry)
    return entry if entry.class == Hash
    { entry => nil }
  end

  def include?(word)
    @definitions.key?(word)
  end

  def find(word)
    @definitions.select do |k, _v|
      k.include?(word)
    end
  end

  def printable
    str = ""
    keywords.each { |k| str << "[#{k}] \"#{@definitions[k]}\"\n" }
    str[0..-2]
  end
end
