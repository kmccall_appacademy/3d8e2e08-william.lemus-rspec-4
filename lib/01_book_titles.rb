class Book
  # TODO: your code goes here!
  attr_reader :title
  def title=(name)
    @title = fix_title(name)
  end

  def fix_title(name)
    lower = ["in", "a", "the", "an", "of", "and"]
    name.split.map.with_index do |word, idx|
      if lower.include?(word) && idx > 0
        word
      else
        word.capitalize
      end
    end.join(" ")
  end
end
